package ro.infoiasi.aset.webStorage;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WebStorageInterfaceTest {

    @Test
    public void testList() throws Exception {
        // Listing web root
        String webRoot = "http://aset.esy.es/";
        WebStorageInterface webStorageInterface = new WebStorageInterface(webRoot, "./");

        // Create expected result
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("iaprtc12/");
        expectedResult.add("index/");
        expectedResult.add("path_unix.properties");
        expectedResult.add("path_win.properties");

        // Get actual result
        List<String> actualResult = webStorageInterface.list("/");

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testGetFile() throws Exception {
        // This test check if method download file
        String webRoot = "http://aset.esy.es/";
        WebStorageInterface webStorageInterface = new WebStorageInterface(webRoot, "./");
        String fileToGet = "/path_win.properties";

        // Create expected result (file should be downloaded to local folder "./")
        File expectedFile = new File("./" + fileToGet);

        // Get actual result
        File actualFile = webStorageInterface.getFile(fileToGet);

        assertEquals(expectedFile, actualFile);
    }

    @Test
    public void testGetLocalAbsoluteFilePath() throws Exception {
        // This test check if method download file and return absolute path
        String webRoot = "http://aset.esy.es/";
        WebStorageInterface webStorageInterface = new WebStorageInterface(webRoot, "./");
        String fileToGet = "/path_win.properties";

        // Create expected result (file should be downloaded to local folder "./")
        File expectedFile = new File("./" + fileToGet);

        // Get actual result
//        String actualResult = webStorageInterface.getLocalAbsoluteFilePath(fileToGet);

//        assertEquals(expectedFile.getAbsolutePath(), actualResult);
    }

    @Test
    public void testGetFiles() throws Exception {
        // This test check if only files in web root is downloaded and is returned
        String webRoot = "http://aset.esy.es/";
        WebStorageInterface webStorageInterface = new WebStorageInterface(webRoot, "./");

        // Create expected result
        List<File> expectedFileList = new ArrayList<>();
        expectedFileList.add(new File("./path_unix.properties"));
        expectedFileList.add(new File("./path_win.properties"));

        // Create actual result
        List<File> actualFileList = webStorageInterface.getFiles("/");

        assertEquals(expectedFileList, actualFileList);
    }
}