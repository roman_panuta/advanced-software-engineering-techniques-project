package ro.infoiasi.aset.queryProcessors;


import org.junit.Test;
import ro.infoiasi.aset.queryprocessors.QueryProcessor;
import ro.infoiasi.aset.queryprocessors.QueryProcessorFactory;
import ro.infoiasi.aset.queryprocessors.StandardQueryProcessor;

import static org.junit.Assert.assertEquals;

public class StandardQueryProcessorTest {

	@Test
	public void testStandardQueryProcessor() {
		QueryProcessor qp = QueryProcessorFactory.initializeStandardQueryProcessor();
		assertEquals(qp.getClass(), StandardQueryProcessor.class);
	}

	@Test
	public void testProcessQuery() {
		String testString = "The birds flew south";
		String expectedString = "the bird fly south";
		QueryProcessor qp = QueryProcessorFactory.initializeStandardQueryProcessor();
		String lemmatizedString = qp.processQuery(testString);
		assertEquals(expectedString, lemmatizedString);
	}

}
