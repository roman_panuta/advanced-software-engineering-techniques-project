package ro.infoiasi.aset.queryProcessors;

import static org.junit.Assert.*;

import org.junit.Test;
import ro.infoiasi.aset.queryprocessors.*;


public class QueryProcessorFactoryTest {

	@Test
	public void testInitializeStandardQueryProcessor() {
		QueryProcessor qp = QueryProcessorFactory.initializeStandardQueryProcessor();
		assertEquals(qp.getClass(), StandardQueryProcessor.class);
	}

	@Test
	public void testInitializeFullQueryProcessor() {
		QueryProcessor qp = QueryProcessorFactory.initializeFullQueryProcessor();
		assertEquals(qp.getClass(), FullQueryProcessor.class);
	}

	@Test
	public void testInitializeSynonymQueryProcessor() {
		QueryProcessor qp = QueryProcessorFactory.initializeSynonymQueryProcessor();
		assertEquals(qp.getClass(), SynonymQueryProcessor.class);
	}


	@Test
	public void testInitializeTranslateQueryProcessor() {
		QueryProcessor qp = QueryProcessorFactory.initializeTranslateQueryProcessor();
		assertEquals(qp.getClass(), TranslateQueryProcessor.class);
	}

}
