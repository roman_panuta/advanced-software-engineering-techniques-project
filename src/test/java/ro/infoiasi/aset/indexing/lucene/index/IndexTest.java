package ro.infoiasi.aset.indexing.lucene.index;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import ro.infoiasi.aset.ApplicationContext;
import ro.infoiasi.aset.indexing.beanUtils.index.Index;
import ro.infoiasi.aset.model.Image;

public class IndexTest {
	@BeforeClass
	public static void setUp(){
		File file = new File("test");
		if(file.exists()) {
			for(File child : file.listFiles()){
				child.delete();
			}
			file.delete();
		}
		ApplicationContext.INDEX_FOLDER = "test";
	}
	
	@Test
	public void addOneImageSearch() throws IOException{
		Index.getInstance().index(image("img1", "location", "iasi"));
		
		List<Image> results = Index.getInstance().search("location:iasi");
		
		assertEquals(1, results.size());
		assertEquals("img1", results.get(0).getName());
	}

	@Test
	public void addOneImageSearchAddOneMoreImage() throws IOException{
		Index.getInstance().index(image("img2", "location", "focsani"));
		
		List<Image> results = Index.getInstance().search("location:focsani");
		
		Index.getInstance().index(image("img3", "title", "TAIP"));
		
		results = Index.getInstance().search("title:taip");
		
		assertEquals(1, results.size());
		assertEquals("img3", results.get(0).getName());
	}
	
	private Image image(String name, String key, String value) {
		Image img = new Image(name);
		img.addMetadata(key, value);
		return img;
	}
}
