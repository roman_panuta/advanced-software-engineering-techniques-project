package ro.infoiasi.aset.indexing.predicate;

import org.junit.Test;
import ro.infoiasi.aset.indexing.engine.query.Filter;
import ro.infoiasi.aset.indexing.engine.query.FilterNotValidException;

import static org.junit.Assert.assertEquals;


public class AndFilterTest {

    @Test(expected=FilterNotValidException.class)
    public void testBuildException() throws Exception {
        Filter filter = new AndFilter(null, null);
        filter.build();
    }

    @Test
    public void testBuild() throws FilterNotValidException {
        String result = "title:lazy dog AND body:quick fox";
        Filter filter = new AndFilter(new ContainsFilter("title", "lazy dog"),
                new ContainsFilter("body", "quick fox"));
        assertEquals(filter.build(), result);
    }
}