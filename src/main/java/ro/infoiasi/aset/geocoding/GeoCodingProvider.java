package ro.infoiasi.aset.geocoding;

public interface GeoCodingProvider {
    public Coordinates getCoordinatesForLocation(String location);
}
