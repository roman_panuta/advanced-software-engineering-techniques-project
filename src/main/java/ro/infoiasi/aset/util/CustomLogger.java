package ro.infoiasi.aset.util;


import ro.infoiasi.aset.ApplicationContext;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

public class CustomLogger {
    private static Integer ctr = 0;
    private CustomLogger() {}
    public static Logger getLogger(String loggerName) {
        Logger logger = Logger.getLogger(loggerName);
        FileHandler fh;

        try {
            fh = new FileHandler(ApplicationContext.LOG_FILE + (++ctr));
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return logger;
    }
}
