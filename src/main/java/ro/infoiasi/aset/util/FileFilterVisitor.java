package ro.infoiasi.aset.util;

import ro.infoiasi.aset.ApplicationContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class FileFilterVisitor {
    public List<File> findFiles(String dirPath, String extension) throws IOException {
        FileListingVisitor fileProcessor = new FileListingVisitor(extension);
        Files.walkFileTree(Paths.get(dirPath), fileProcessor);
        return fileProcessor.getFiles();
    }

    private static final class FileListingVisitor extends SimpleFileVisitor<Path> {
        private final List<File> files;
        private final String extension;

        public FileListingVisitor(String extension) {
            this.extension = extension;
            this.files = new ArrayList<>();
        }

        public List<File> getFiles() {
            return files;
        }

        @Override
        public FileVisitResult visitFile(Path filePath, BasicFileAttributes basicFileAttributes) throws IOException {
            if(filePath.toString().endsWith(ApplicationContext.METADATA_EXTENSION)) {
                files.add(filePath.toFile());
            }
            return FileVisitResult.CONTINUE;
        }
    }
}
