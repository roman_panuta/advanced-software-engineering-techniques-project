package ro.infoiasi.aset.util.batchProcessor;

public interface ProcessingTask<T>{
    public abstract void setUp() throws Exception;
    public abstract T call() throws Exception;
    public abstract void tearDown() throws Exception;
}
