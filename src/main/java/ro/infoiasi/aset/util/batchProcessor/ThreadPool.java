package ro.infoiasi.aset.util.batchProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ThreadPool<T> {
    private ExecutorService threadPool;
    private ExecutorCompletionService<T> service;
    private boolean canSubmit = true;
    List<Future<T>> futures = new ArrayList<>();

    public ThreadPool() {
        threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        service = new ExecutorCompletionService<T>(threadPool);
    }

    public void submit(ProcessingTask<T> task) {
        if (!canSubmit) {
            throw new IllegalStateException("Thread Pool has been shut down");
        }
        futures.add(service.submit(new TaskCallable<T>(task)));
    }

    public void submit(List<ProcessingTask<T>> tasks) {
        if (!canSubmit) {
            throw new IllegalStateException("Thread Pool has been shut down");
        }
        for (ProcessingTask<T> task : tasks) {
            submit(task);
        }
    }

    public void shutdown() {
        if (canSubmit) {
            threadPool.shutdown();
            canSubmit = false;
        }
        System.out.println("Shutting down...");
    }

    public void waitToFinish() {
        if (canSubmit) {
            throw new IllegalStateException("You must first shut-down the thread pool");
        }
        while (!threadPool.isTerminated());
        System.out.println("Tasks finished");
    }

    /**
     * Waits for all tasks to finish and collects the results
     * @return the list of Ts
     */
    public List<T> getResults() {
        waitToFinish();
        System.out.println("Constructing resuts");
        List<T> documents = new ArrayList<>();
        for(Future<T> future: futures) {
            try {
                documents.add(future.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return documents;
    }
}