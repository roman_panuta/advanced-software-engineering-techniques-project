package ro.infoiasi.aset.util.batchProcessor;

import java.util.concurrent.Callable;

public class TaskCallable<V> implements Callable<V> {
    private final ProcessingTask<V> processingTask;

    public TaskCallable(ProcessingTask<V> processingTask) {
        this.processingTask = processingTask;
    }

    @Override
    public V call() throws Exception {
        processingTask.setUp();
        V result = processingTask.call();
        processingTask.tearDown();
        return result;
    }
}
