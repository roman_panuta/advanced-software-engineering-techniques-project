package ro.infoiasi.aset.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ro.infoiasi.aset.indexing.engine.query.FilterNotValidException;
import ro.infoiasi.aset.indexing.predicate.ContainsFilter;
import ro.infoiasi.aset.indexing.predicate.OrFilter;
import ro.infoiasi.aset.main.ImageSearcherUtil;
import ro.infoiasi.aset.model.Image;
import ro.infoiasi.aset.queryprocessors.QueryProcessor;
import ro.infoiasi.aset.queryprocessors.QueryProcessorFactory;

@ManagedBean(name="imageBean")
@SessionScoped
public class ImageBean {
	public static final String HOST = "http://aset.esy.es/iaprtc12/";
	private QueryProcessor queryProcessor;
	private String searchExpresion;
	private ImageSearcherUtil imageSearcher;
	private List<String> imagesPath = new ArrayList<String>();

	public ImageBean() {
		imageSearcher = new ImageSearcherUtil();
		this.queryProcessor = QueryProcessorFactory.initializeStandardQueryProcessor();
	}

	public void search(){
		if(searchExpresion != null){
			try {
				searchExpresion = queryProcessor.processQuery(searchExpresion);
				ContainsFilter titleFilter = new ContainsFilter(Image.MetaModel.TITLE,searchExpresion);
				ContainsFilter descriptionFilter = new ContainsFilter(Image.MetaModel.DESCRIPTION, searchExpresion);
				OrFilter orFilter = new OrFilter(titleFilter, descriptionFilter);
				imagesPath = imageSearcher.search(orFilter.build());
			} catch (IOException | FilterNotValidException e) {
				e.printStackTrace();
			}
		}
	}

	public String getSearchExpresion() {
		return searchExpresion;
	}

	public void setSearchExpresion(String searchExpresion) {
		this.searchExpresion = searchExpresion;
	}

	public List<String> getImagesPath() {
		return imagesPath;
	}

	public void setImagesPath(List<String> imagesPath) {
		this.imagesPath = imagesPath;
	}

	public String getUrl(String relPath) {
		return HOST + relPath;
	}

}
