package ro.infoiasi.aset.indexing.converter;

import ro.infoiasi.aset.metadata.imageclef.ImageClefModel;
import ro.infoiasi.aset.model.Image;

public class Converter {

    private static Image createImage(String name, String... metadata) {
        Image image = new Image(name);
        for(int i = 0; i < metadata.length; i += 2){
            image.addMetadata(metadata[i], metadata[i+1]);
        }
        return image;
    }

    public static Image toImage(ImageClefModel imageClef) {
        return createImage(imageClef.getImage(),
                Image.MetaModel.DOCNO, imageClef.getNameOnDisk(),
                Image.MetaModel.TITLE, imageClef.getTitle(),
                Image.MetaModel.DESCRIPTION, imageClef.getDescription(),
                Image.MetaModel.LOCATION, imageClef.getLocation()
        );
    }
}
