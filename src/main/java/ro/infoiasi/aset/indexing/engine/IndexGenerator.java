package ro.infoiasi.aset.indexing.engine;

import ro.infoiasi.aset.model.Image;

public interface IndexGenerator {
    public void index(Image image);
}
