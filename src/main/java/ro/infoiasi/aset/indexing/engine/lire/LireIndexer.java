package ro.infoiasi.aset.indexing.engine.lire;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import ro.infoiasi.aset.indexing.factories.IndexWriterFactory;

import java.io.IOException;

public class LireIndexer {
    private IndexWriter indexWriter;
    private final StandardAnalyzer analyzer;
    private final IndexWriterConfig config;
    private final Directory directory;

    {
        analyzer = new StandardAnalyzer(Version.LUCENE_40);
        config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
    }

    public LireIndexer(Directory directory) throws IOException {
        this.directory = directory;
        indexWriter = IndexWriterFactory.getIndexWriterInstance(directory, config);
    }

    public void index(Document document) {
        try {
            indexWriter.addDocument(document);
        } catch (IOException e) {
            //TODO: aop exception logging
            e.printStackTrace();
        }
    }
}