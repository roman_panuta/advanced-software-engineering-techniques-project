package ro.infoiasi.aset.indexing.engine.lucene;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import ro.infoiasi.aset.indexing.engine.IndexGenerator;
import ro.infoiasi.aset.indexing.factories.IndexWriterFactory;
import ro.infoiasi.aset.indexing.beanUtils.startegy.IndexingStrategy;
import ro.infoiasi.aset.indexing.beanUtils.startegy.TextFieldIndexing;
import ro.infoiasi.aset.model.Image;

import java.io.IOException;

public class LuceneIndexGenerator implements IndexGenerator {
    private final IndexingStrategy strategy;
    private IndexWriter indexWriter;

    private LuceneIndexGenerator(IndexWriterConfig config, Directory directory, IndexingStrategy strategy) throws IOException {
        indexWriter = IndexWriterFactory.getIndexWriterInstance(directory, config);
        this.strategy = strategy;
    }

    @Override
    public void index(Image image) {
        try {
            Document document = strategy.doIndex(image);
            if(document != null) {
                indexWriter.addDocument(document);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void commit() {
        try {
            indexWriter.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Builder for Index Generator
     */
    public static class Builder {
        private IndexWriterConfig config =new IndexWriterConfig(Version.LUCENE_4_10_2,
                                                                new StandardAnalyzer(Version.LUCENE_4_10_2));
        IndexingStrategy indexingStrategy = new TextFieldIndexing();
        Directory directory;

        public Builder(Directory directory) {
            this.directory = directory;
        }

        public Builder setConfig(IndexWriterConfig config) {
            this.config = config;
            return this;
        }

        public Builder setIndexingStrategy(IndexingStrategy indexingStrategy) {
            this.indexingStrategy = indexingStrategy;
            return this;
        }

        public LuceneIndexGenerator build() throws IOException {
            return new LuceneIndexGenerator(config, directory, indexingStrategy);
        }

    }
}
