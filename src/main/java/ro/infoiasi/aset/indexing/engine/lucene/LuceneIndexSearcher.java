package ro.infoiasi.aset.indexing.engine.lucene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import ro.infoiasi.aset.indexing.engine.IndexEngine;
import ro.infoiasi.aset.indexing.engine.query.Filter;
import ro.infoiasi.aset.indexing.engine.query.FilterNotValidException;
import ro.infoiasi.aset.indexing.factories.IndexReaderFactory;
import ro.infoiasi.aset.indexing.storage.factory.FactoryNotFoundException;
import ro.infoiasi.aset.model.Image;
public class LuceneIndexSearcher implements IndexEngine{
	private Directory directory;

	public LuceneIndexSearcher(Directory directory) throws IOException, FactoryNotFoundException {
		this.directory = directory;
	}
	
	private List<Image> search(String query){
		try {
			return trySearch(query);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	private List<Image> trySearch(String query) throws ParseException, IOException {
		QueryParser queryParser = new QueryParser(Version.LUCENE_40, "", new StandardAnalyzer(Version.LUCENE_40));
		IndexSearcher searcher = new IndexSearcher(IndexReaderFactory.getInstance(directory));
		Query q = queryParser.parse(query);

		TopScoreDocCollector collector = TopScoreDocCollector.create(10, true);
		searcher.search(q, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		
		List<Image> results = new ArrayList<Image>();
		for(ScoreDoc sc : hits){
			int id = sc.doc;
			Document doc = searcher.doc(id);
			results.add(new Image(doc.get("name")));
		}
		
		return results;
	}

	@Override
	public List<Image> search(Filter filter) {
		try {
			return search(filter.build());
		} catch (FilterNotValidException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}
}