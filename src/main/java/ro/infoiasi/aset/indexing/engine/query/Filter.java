package ro.infoiasi.aset.indexing.engine.query;

public interface Filter {
    public String build() throws FilterNotValidException;
}
