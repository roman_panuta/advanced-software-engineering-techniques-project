package ro.infoiasi.aset.indexing.engine;

import ro.infoiasi.aset.indexing.engine.query.Filter;
import ro.infoiasi.aset.model.Image;

import java.util.List;

public interface IndexEngine {
    public List<Image> search(Filter criteria);
}
