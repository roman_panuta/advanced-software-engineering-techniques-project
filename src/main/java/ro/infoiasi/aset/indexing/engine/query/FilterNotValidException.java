package ro.infoiasi.aset.indexing.engine.query;

public class FilterNotValidException extends Exception {
    public FilterNotValidException() {
        super("Filter is not valid, possible null values");
    }
}
