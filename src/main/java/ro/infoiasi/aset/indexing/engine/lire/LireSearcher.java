package ro.infoiasi.aset.indexing.engine.lire;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;
import net.semanticmetadata.lire.imageanalysis.CEDD;
import net.semanticmetadata.lire.impl.GenericFastImageSearcher;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.FSDirectory;
import ro.infoiasi.aset.research.in.ImageData;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LireSearcher {
    private IndexReader indexReader;
    private ImageSearcher imageSearcher;

    public LireSearcher(String indexPath, int count) throws IOException {
        indexReader = DirectoryReader.open(FSDirectory.open(new File(indexPath)));
        imageSearcher = new GenericFastImageSearcher(count, CEDD.class);
    }

    public LireSearcher(String indexPath, Class<?> descriptorClass, int count) throws IOException {
        indexReader = DirectoryReader.open(FSDirectory.open(new File(indexPath)));
        imageSearcher = new GenericFastImageSearcher(count, descriptorClass);
    }

    public List<String> search(String imagePath) {
        List<String> result = new ArrayList<>();
        BufferedImage img;
        File f = new File(imagePath);
        if (f.exists()) {
            try {
                img = ImageIO.read(f);
                ImageSearchHits hits = imageSearcher.search(img, indexReader);

                for (int i = 0; i < hits.length(); i++) {
                    String fileName = hits.doc(i).getValues(DocumentBuilder.FIELD_NAME_IDENTIFIER)[0];
                    //System.out.println(hits.score(i) + ": \t" + fileName);
                    result.add(fileName);
                }
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return result;
    }

    public List<ImageData> debugSearch(String imagePath) {
        List<ImageData> result = new ArrayList<>();
        BufferedImage img;
        File f = new File(imagePath);
        if (f.exists()) {
            try {
                img = ImageIO.read(f);
                ImageSearchHits hits = imageSearcher.search(img, indexReader);

                for (int i = 0; i < hits.length(); i++) {
                    String fileName = hits.doc(i).getValues(DocumentBuilder.FIELD_NAME_IDENTIFIER)[0];
                    result.add(new ImageData(fileName, i, hits.score(i)));
                }
            } catch (IOException e) {
                //To change body of catch statement use File | Settings | File Templates.
                e.printStackTrace();
            }
        }
        return result;
    }
}
