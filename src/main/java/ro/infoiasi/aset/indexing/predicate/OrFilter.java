package ro.infoiasi.aset.indexing.predicate;

import ro.infoiasi.aset.indexing.engine.query.Filter;
import ro.infoiasi.aset.indexing.engine.query.FilterNotValidException;

public class OrFilter implements Filter{
    private Filter lhs;
    private Filter rhs;

    public OrFilter(Filter lhs, Filter rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public String build() throws FilterNotValidException {
        if(lhs == null || rhs == null) {
            throw new FilterNotValidException();
        }
        return lhs.build() + " OR " + rhs.build();
    }
}
