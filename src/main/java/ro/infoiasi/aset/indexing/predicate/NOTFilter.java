package ro.infoiasi.aset.indexing.predicate;

import ro.infoiasi.aset.indexing.engine.query.Filter;

public class NOTFilter implements Filter{
    private final String field;
    private final String include;
    private final String exclude;

    public NOTFilter(String field, String includePhrase, String excludePhrase) {
        this.field = field;
        this.include = includePhrase;
        this.exclude = excludePhrase;
    }

    @Override
    public String build() {
        return new ContainsFilter(field,include).build() + " -" + new ContainsFilter(field, exclude).build();
    }
}
