package ro.infoiasi.aset.indexing.predicate;

import ro.infoiasi.aset.indexing.engine.query.Filter;
import ro.infoiasi.aset.indexing.engine.query.FilterNotValidException;

public class BoostFilter implements Filter{
    private Filter lhs;
    private float boost;

    public BoostFilter(Filter lhs, float boost) {
        this.lhs = lhs;
        this.boost = boost;
    }

    @Override
    public String build() throws FilterNotValidException {
        if(lhs == null) {
            throw new FilterNotValidException();
        }
        return "(" + lhs.build() + ")" + "^" +boost;
    }
}
