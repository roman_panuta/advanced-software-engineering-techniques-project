package ro.infoiasi.aset.indexing.predicate;

import ro.infoiasi.aset.indexing.engine.query.Filter;

public class ContainsFilter  implements Filter{
    private String phrase;
    private String field;

    public ContainsFilter(String field, String phrase) {
        this.phrase = phrase;
        this.field = field;
    }

    public String build() {
        return field + ":"+phrase;
    }
}
