package ro.infoiasi.aset.indexing.batchProcess;

import org.apache.lucene.store.Directory;
import ro.infoiasi.aset.ApplicationContext;
import ro.infoiasi.aset.indexing.task.IndexingTask;
import ro.infoiasi.aset.util.batchProcessor.ThreadPool;
import ro.infoiasi.aset.indexing.converter.Converter;
import ro.infoiasi.aset.indexing.engine.lucene.LuceneIndexGenerator;
import ro.infoiasi.aset.indexing.beanUtils.startegy.ImageIndexingStrategy;
import ro.infoiasi.aset.indexing.storage.DirectoryAbstractFactory;
import ro.infoiasi.aset.indexing.storage.StorageType;
import ro.infoiasi.aset.indexing.storage.factory.FactoryNotFoundException;
import ro.infoiasi.aset.indexing.task.UnmarshallingTask;
import ro.infoiasi.aset.metadata.imageclef.ImageClefModel;
import ro.infoiasi.aset.util.FileFilterVisitor;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class FileIndexer {
    private final String annotationsPath;
    private final String indexDestinationPath;

    public FileIndexer(String annotationPath, String indexDestinationPath) {
        this.annotationsPath = annotationPath;
        this.indexDestinationPath = indexDestinationPath;
    }

    private List<File> getFiles() {
        FileFilterVisitor filterVisitor = new FileFilterVisitor();
        try {
            return filterVisitor.findFiles(annotationsPath, ApplicationContext.METADATA_EXTENSION);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Directory getDirecoty() {
        try {
            return DirectoryAbstractFactory.getDirectory(StorageType.FileSystem).createDirectory(indexDestinationPath);
        } catch (IOException | FactoryNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<ImageClefModel> getImageSet() {
        List<File> files = getFiles();
        if(files == null) {
            //TODO: signal fail
            return null;
        }
        // Only for progress
        Collections.sort(files);
        ThreadPool<ImageClefModel> threadPool = new ThreadPool<>();
        for(File f: files) {
            threadPool.submit(new UnmarshallingTask(f));
        }
        threadPool.shutdown();
        return threadPool.getResults();
    }

    private void indexImages(List<ImageClefModel> imageClefModels) throws IOException {
        LuceneIndexGenerator generator = null;
        Directory directory = getDirecoty();
        if(directory == null) {
            return;
        }
        try {
            generator = new LuceneIndexGenerator.Builder(directory)
                    .setIndexingStrategy(new ImageIndexingStrategy()).build();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return;
        }
        ThreadPool<Void> threadPool = new ThreadPool<>();
        for(ImageClefModel imageClefModel: imageClefModels) {
            threadPool.submit(new IndexingTask(imageClefModel, generator));
        }
        threadPool.shutdown();
        // Wait until all the tasks have ended
        threadPool.waitToFinish();
        generator.commit();
    }

    /**
     * @throws IOException
     */
    public void start() throws IOException {
        List<ImageClefModel> imageClefModels = getImageSet();
        if(imageClefModels == null) {
            System.out.println("Image set is null");
            return;
        }
        indexImages(imageClefModels);
    }
}
