package ro.infoiasi.aset.indexing.storage;

import ro.infoiasi.aset.indexing.storage.factory.DirectoryFactory;
import ro.infoiasi.aset.indexing.storage.factory.FSADirectoryFactory;
import ro.infoiasi.aset.indexing.storage.factory.FactoryNotFoundException;
import ro.infoiasi.aset.indexing.storage.factory.RAMDirectoryFactory;

import java.util.HashMap;
import java.util.Map;

public class DirectoryAbstractFactory {
    private static Map<StorageType, DirectoryFactory> factories = new HashMap<>();

    static {
        register(StorageType.RAM, new RAMDirectoryFactory());
        register(StorageType.FileSystem, new FSADirectoryFactory());
    }

    public static void register(StorageType storageType, DirectoryFactory directoryFactory) {
        factories.put(storageType,directoryFactory);
    }

    public static DirectoryFactory getDirectory(StorageType type) throws FactoryNotFoundException {
        DirectoryFactory factory = factories.get(type);
        if(factory == null) {
            throw new FactoryNotFoundException();
        }
        return factory;
    }
}
