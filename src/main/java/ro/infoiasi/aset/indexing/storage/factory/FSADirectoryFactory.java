package ro.infoiasi.aset.indexing.storage.factory;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;

public class FSADirectoryFactory implements DirectoryFactory {
    public Directory createDirectory(String folder) throws IOException {
        File indexDirectory = new File(folder);
        if (!indexDirectory.exists()) {
            indexDirectory.mkdirs();
        }
        return FSDirectory.open(indexDirectory);
    }
}