package ro.infoiasi.aset.indexing.storage.factory;

import java.io.IOException;

import org.apache.lucene.store.Directory;

public interface DirectoryFactory {
	public abstract Directory createDirectory(String folder) throws IOException;
}
