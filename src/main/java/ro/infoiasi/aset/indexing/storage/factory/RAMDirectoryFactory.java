package ro.infoiasi.aset.indexing.storage.factory;

import java.io.IOException;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class RAMDirectoryFactory implements DirectoryFactory {
	@Override
	public Directory createDirectory(String folder) throws IOException {
		return new RAMDirectory();
	}
}
