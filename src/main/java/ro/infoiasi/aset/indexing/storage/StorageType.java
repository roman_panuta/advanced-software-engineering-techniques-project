package ro.infoiasi.aset.indexing.storage;

public enum StorageType {
    FileSystem, RAM, Cloud
}
