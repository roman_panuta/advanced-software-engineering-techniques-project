package ro.infoiasi.aset.indexing.storage.factory;

public class FactoryNotFoundException extends Exception {

    public FactoryNotFoundException() {
        super("Factory not found exception");
    }
}
