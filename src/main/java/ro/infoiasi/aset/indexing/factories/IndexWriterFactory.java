package ro.infoiasi.aset.indexing.factories;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class IndexWriterFactory {
    private static Map<Directory, IndexWriter> instances = new ConcurrentHashMap<>();

    private IndexWriterFactory() {

    }

    public static IndexWriter getIndexWriterInstance(Directory directory, IndexWriterConfig config) throws IOException {
        IndexWriter indexWriter = instances.get(directory);
        if(indexWriter == null) {
            indexWriter = new IndexWriter(directory, config);
            instances.put(directory,indexWriter);
        }
        return indexWriter;
    }

    public static void closeAll() {
        try {
            for(IndexWriter iw: instances.values()) {
                iw.close();
            }
        }catch (IOException e) {
                e.printStackTrace();
        }
    }
}
