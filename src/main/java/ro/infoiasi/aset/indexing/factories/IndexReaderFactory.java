package ro.infoiasi.aset.indexing.factories;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Don't use this class if you need to refresh the reader (in order to show the new images added by the writer)
 */
public class IndexReaderFactory {
    private static Map<Directory, IndexReader> instances = new ConcurrentHashMap<>();

    /**
     * This method returns a single instance for a specific directory
     * @param directory  The index directory. The index is either created or appended according
     * @return An instance of index Reader
     */
    public static synchronized IndexReader getInstance(Directory directory) throws IOException {
        IndexReader reader = instances.get(directory);
        if(reader == null) {
            reader = DirectoryReader.open(directory);
            instances.put(directory,reader);
        }
        return reader;
    }


    public static void closeAll() {
        try {
            for(IndexReader ir: instances.values()) {
                ir.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
