package ro.infoiasi.aset.indexing.task;

import ro.infoiasi.aset.util.batchProcessor.ProcessingTask;
import ro.infoiasi.aset.metadata.MetaDataReader;
import ro.infoiasi.aset.metadata.imageclef.ImageClefModel;
import ro.infoiasi.aset.metadata.imageclef.ImageClefXMLReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class UnmarshallingTask implements ProcessingTask<ImageClefModel> {
    private final File file;
    private MetaDataReader<ImageClefModel> reader;

    public UnmarshallingTask(File file) {
        this.file = file;
    }

    @Override
    public void setUp() {
        //TODO: add geoCoding component
        reader = new ImageClefXMLReader();
    }

    @Override
    public ImageClefModel call() throws IOException {
        FileInputStream fileInputStream = null;
        ImageClefModel result = null;
        fileInputStream = new FileInputStream(file);
        result= reader.loadFile(fileInputStream);
        fileInputStream.close();
        return result;
    }

    @Override
    public void tearDown() {
        reader = null;
        //nothing to do at the moment
    }
}
