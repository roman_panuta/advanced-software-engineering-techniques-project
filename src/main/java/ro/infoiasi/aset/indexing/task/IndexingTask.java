package ro.infoiasi.aset.indexing.task;

import ro.infoiasi.aset.indexing.converter.Converter;
import ro.infoiasi.aset.indexing.engine.lucene.LuceneIndexGenerator;
import ro.infoiasi.aset.metadata.MetaDataReader;
import ro.infoiasi.aset.metadata.imageclef.ImageClefModel;
import ro.infoiasi.aset.metadata.imageclef.ImageClefXMLReader;
import ro.infoiasi.aset.util.batchProcessor.ProcessingTask;

import java.io.File;
import java.io.FileInputStream;

public class IndexingTask implements ProcessingTask<Void> {
    private final LuceneIndexGenerator generator;
    private final ImageClefModel model;

    public IndexingTask(ImageClefModel imageClefModel, LuceneIndexGenerator generator) {
        this.model = imageClefModel;
        this.generator = generator;
    }

    @Override
    public void setUp() {

    }

    @Override
    public Void call() {
        generator.index(Converter.toImage(model));
        return null;
    }

    @Override
    public void tearDown() {
        //nothing to do at the moment
    }
}
