package ro.infoiasi.aset.indexing.beanUtils.index;

import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import ro.infoiasi.aset.ApplicationContext;
import ro.infoiasi.aset.indexing.factories.IndexWriterFactory;
import ro.infoiasi.aset.indexing.storage.DirectoryAbstractFactory;
import ro.infoiasi.aset.indexing.storage.StorageType;
import ro.infoiasi.aset.indexing.storage.factory.FactoryNotFoundException;

public class IndexService {
    private StandardAnalyzer analyzer;
    private IndexWriterConfig config;
    private Directory directory;

    public IndexService() {
        try {
            directory = DirectoryAbstractFactory.getDirectory(StorageType.FileSystem).createDirectory(ApplicationContext.INDEX_FOLDER);
            analyzer = new StandardAnalyzer(Version.LUCENE_40);
            config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
        } catch (IOException | FactoryNotFoundException e) {
            e.printStackTrace();
        }
    }

    public IndexWriter createIndexWriter() throws IOException {
        return IndexWriterFactory.getIndexWriterInstance(directory, config);
    }

    public IndexReader createIndexReader() throws IOException {
    	return DirectoryReader.open(directory);
    }
}
