package ro.infoiasi.aset.indexing.beanUtils.startegy;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import ro.infoiasi.aset.model.Image;

import java.util.Map;

public class TextFieldIndexing implements IndexingStrategy{
    @Override
    public Document doIndex(Image image) {
        Document document = new Document();
        document.add(new StringField("name", image.getName(), Field.Store.YES));

        for(Map.Entry<String, String> entry : image.getMetadata().entrySet()){
            document.add(new TextField(entry.getKey(), entry.getValue(), Field.Store.NO));
        }

        return document;
    }
}
