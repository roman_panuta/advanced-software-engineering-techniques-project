package ro.infoiasi.aset.indexing.beanUtils.startegy;

import org.apache.lucene.document.Document;
import ro.infoiasi.aset.model.Image;

public interface IndexingStrategy {
    public Document doIndex(Image image);
}
