package ro.infoiasi.aset.indexing.beanUtils.startegy;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.DocumentBuilderFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import ro.infoiasi.aset.ApplicationContext;
import ro.infoiasi.aset.model.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class ImageIndexingStrategy implements IndexingStrategy{
    @Override
    public Document doIndex(Image image) {
        try {
            DocumentBuilder dbf = DocumentBuilderFactory.getFullDocumentBuilder();
            Path p = Paths.get(ApplicationContext.IMAGE_CLEF_FOLDER, image.getName());
            Document document = dbf.createDocument(getBufferedImage(p.toFile()), image.getName());
            document.add(new StringField("name", image.getName(), Field.Store.YES));
            for(Map.Entry<String, String> entry : image.getMetadata().entrySet()){
                document.add(new TextField(entry.getKey(), entry.getValue(), Field.Store.NO));
            }
            return document;
        } catch (IOException e) {
            return null;
        }
    }

    public BufferedImage getBufferedImage(File image) throws IOException {
        return ImageIO.read(image);
    }
}
