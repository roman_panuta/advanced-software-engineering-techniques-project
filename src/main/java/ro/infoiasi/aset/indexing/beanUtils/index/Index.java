package ro.infoiasi.aset.indexing.beanUtils.index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.util.Version;

import ro.infoiasi.aset.model.Image;


public class Index {
	private static Index instance;
	
	public static Index getInstance() throws IOException{
		if(instance == null){
			synchronized (Index.class) {
				if(instance == null) {
					instance = new Index();
				}
			}
		}
		return instance;
	}
	
	private IndexWriter indexWriter;
	private IndexReader indexReader;
	
//	private DirectoryFactory factory;
	private IndexService indexService;
	private boolean commit;
	
	private Index() throws IOException {
//		factory = new FSDirectoryFactory();
		indexService = new IndexService();
		
//		indexWriter = factory.createIndexWriter();
		indexWriter = indexService.createIndexWriter();
		commit = false;
		//indexReader = factory.createIndexReader();
	}
	
	public List<Image> search(String query){
		try {
			return trySearch(query);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e1){
			e1.printStackTrace();
		}
		return Collections.emptyList();
	}

	public void index(Image image) {
		try {
			tryIndex(image);
		} catch (IOException e) {
			e.printStackTrace();
		}	
//		return this;
	}
	
	private List<Image> trySearch(String query) throws ParseException,
			IOException {
		QueryParser queryParser = new QueryParser(Version.LUCENE_40, "", new StandardAnalyzer(Version.LUCENE_40));
		IndexSearcher searcher = new IndexSearcher(getIndexReader());
		
		TopScoreDocCollector collector = TopScoreDocCollector.create(10, true);
		Query q = queryParser.parse(query);
		searcher.search(q, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		
		List<Image> results = new ArrayList<Image>();
		for(ScoreDoc sc : hits){
			int id = sc.doc;
			Document doc = searcher.doc(id);
			results.add(new Image(doc.get("name")));
		}
		
		return results;
	}

	private void tryIndex(Image image) throws IOException {
		Document doc = new Document();
		doc.add(new TextField("name", image.getName(), Field.Store.YES));
		
//		image.getMetadata()
//			.forEach((k, v) -> doc.add(new TextField(k, v, Field.Store.NO)));
		
		for(Map.Entry<String, String> entry : image.getMetadata().entrySet()){
			doc.add(new TextField(entry.getKey(), entry.getValue(), Field.Store.NO));
		}
		
		indexWriter.addDocument(doc);
		commit = false;
	}
	
	private IndexReader getIndexReader() throws IOException{
		if(indexReader == null || !commit){
			indexWriter.commit();
			commit = true;
			
//			indexReader = factory.createIndexReader();
			indexReader = indexService.createIndexReader();
		}
		return indexReader;
	}
}