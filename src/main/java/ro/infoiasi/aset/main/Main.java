package ro.infoiasi.aset.main;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import net.semanticmetadata.lire.imageanalysis.*;
import net.semanticmetadata.lire.imageanalysis.joint.JointHistogram;
import ro.infoiasi.aset.indexing.engine.lire.LireSearcher;
import ro.infoiasi.aset.indexing.engine.query.FilterNotValidException;
import ro.infoiasi.aset.indexing.storage.factory.FactoryNotFoundException;
import ro.infoiasi.aset.ApplicationContext;
import ro.infoiasi.aset.metadata.imageclef.ImageClefXMLReader;
import ro.infoiasi.aset.queryprocessors.QueryProcessorFactory;
import ro.infoiasi.aset.research.in.ImageData;
import ro.infoiasi.aset.research.out.ImageInfo;
import ro.infoiasi.aset.research.out.ImageRow;
import ro.infoiasi.aset.research.out.Table;

public class Main {

	static {
		String path = "/home/crisstian/Facultate/ASET/src/main/webapp/WEB-INF/config/path.properties";
		String wnPath = "/home/crisstian/Facultate/ASET/src/main/webapp/WEB-INF/resources/wordNet/dict";
		try {
			ApplicationContext.init(path, wnPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException, FactoryNotFoundException, FilterNotValidException {
		String testImage = "/home/crisstian/Facultate/ASET/assets/test 1.jpg";

		QueryProcessorFactory.initializeSynonymQueryProcessor().processQuery("cat");
	}

	public static void crossSearch(String testImage, int imgCount) throws IOException {
		Table table = new Table();
		Class<?> classes[] = {CEDD.class, ColorLayout.class, EdgeHistogram.class, ScalableColor.class,
				AutoColorCorrelogram.class, FCTH.class, SimpleColorHistogram.class,
				Tamura.class, Gabor.class, JointHistogram.class};
		for(Class<?> descriptorClass: classes) {
			LireSearcher lireSearcher = new LireSearcher(ApplicationContext.LIRE_INDEX_FOLDER, descriptorClass, imgCount);
			List<ImageData> dataList = lireSearcher.debugSearch(testImage);
			for(ImageData imageData: dataList) {
				table.addImage(imageData, descriptorClass);
			}
		}
		System.out.println(CSVOutput(table,classes));
	}

	public static String CSVOutput(Table table, Class<?>[] classes) {
		StringBuilder output = new StringBuilder("");
		output.append("Name,");
		for(Class<?> descriptorClass: classes) {
			output.append(descriptorClass.getName().replace("net.semanticmetadata.lire.imageanalysis.", "") + ",");
		}
		output.append("\r\n");
		Set<String> images = table.getAllImages();
		for(String image: images) {
			output.append(image);
			output.append(",");
			ImageRow imageRow = table.getImageRow(image);
			for(Class<?> descriptorClass: classes) {
				ImageInfo imageInfo = imageRow.getImageData(descriptorClass);
				if(imageInfo != null) {
					output.append(imageInfo.getPosition());
				} else {
					output.append("101");
				}
				output.append(",");
			}
			output.append("\r\n");
		}
		return output.toString();
	}

	public static String markDownOutput(Table table, Class<?>[] classes) {
		StringBuilder output = new StringBuilder("");
		output.append("| Name |");
		for(Class<?> descriptorClass: classes) {
			output.append(descriptorClass.getName().replace("net.semanticmetadata.lire.imageanalysis.", "") + "|");
		}
		output.append("\r\n");
		Set<String> images = table.getAllImages();
		for(String image: images) {
			output.append("|");
			output.append(image);
			output.append("|");
			ImageRow imageRow = table.getImageRow(image);
			for(Class<?> descriptorClass: classes) {
				ImageInfo imageInfo = imageRow.getImageData(descriptorClass);
				if(imageInfo != null) {
					output.append(imageInfo.getPosition());
				} else {
					output.append("101");
				}
				output.append("|");
			}
			output.append("\r\n");
		}
		return output.toString();
	}

}
