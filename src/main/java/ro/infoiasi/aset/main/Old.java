package ro.infoiasi.aset.main;

import ro.infoiasi.aset.ApplicationContext;
import ro.infoiasi.aset.indexing.batchProcess.FileIndexer;
import ro.infoiasi.aset.indexing.engine.lire.LireSearcher;
import ro.infoiasi.aset.indexing.engine.query.FilterNotValidException;
import ro.infoiasi.aset.indexing.predicate.ContainsFilter;
import ro.infoiasi.aset.indexing.predicate.OrFilter;
import ro.infoiasi.aset.indexing.storage.factory.FactoryNotFoundException;
import ro.infoiasi.aset.model.Image;

import java.io.IOException;
import java.util.List;


public class Old {
    private static void luceneSearch() throws FactoryNotFoundException, IOException, FilterNotValidException {
        String searchExpresion = "landscape";
        ContainsFilter titleFilter = new ContainsFilter(Image.MetaModel.TITLE,searchExpresion);
        ContainsFilter descriptionFilter = new ContainsFilter(Image.MetaModel.DESCRIPTION, searchExpresion);
        OrFilter orFilter = new OrFilter(titleFilter, descriptionFilter);
        List<String> images = new ImageSearcherUtil().search(orFilter.build());
        for(String s: images) {
            System.out.println(s);
        }
    }

    private static void generateImageIndex() throws IOException, FactoryNotFoundException {
        FileIndexer indexer = new FileIndexer(ApplicationContext.IMAGE_CLEF_METADATA_FOLDER,ApplicationContext.INDEX_FOLDER);
        indexer.start();
    }

    private static void lireGenerateIndex() throws IOException {
        FileIndexer indexer = new FileIndexer(ApplicationContext.IMAGE_CLEF_METADATA_FOLDER,ApplicationContext.LIRE_INDEX_FOLDER);
        indexer.start();
    }

    private static void lireSearch() throws IOException {
        String testImage = "D:/laborator/ASET/testImage.jpg";
        List<String> lireResult = new LireSearcher(ApplicationContext.LIRE_INDEX_FOLDER, 25).search(testImage);
        lireResult.forEach(System.out::println);
    }

}
