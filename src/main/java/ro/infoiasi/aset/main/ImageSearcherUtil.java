package ro.infoiasi.aset.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ro.infoiasi.aset.indexing.beanUtils.index.Index;
import ro.infoiasi.aset.model.Image;

public class ImageSearcherUtil {

    public List<String> search(String searchExpresion) throws IOException {
        List<String> imagesPath = new ArrayList<String>();
        List<Image> result = Index.getInstance().search(searchExpresion);
        for (Image i : result) {
            imagesPath.add(i.getName());
        }
        return imagesPath;
    }

}

