package ro.infoiasi.aset.queryprocessors;

import java.util.ArrayList;
import java.util.List;

public class TranslateQueryProcessor implements QueryProcessor {
    
    // Translates, then lemmatizes query
    protected TranslateQueryProcessor() throws Exception {
        
    }
    
    @Override
    public String processQuery(String query) {
        
        return query;
    }

	@Override
	public List<List<String>> fullProcessQuery(String query) {
		return new ArrayList<>();
	}
    
}
