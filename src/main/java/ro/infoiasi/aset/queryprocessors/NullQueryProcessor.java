package ro.infoiasi.aset.queryprocessors;

import java.util.ArrayList;
import java.util.List;

public class NullQueryProcessor implements QueryProcessor {

    // Returns the unprocessed query
    protected NullQueryProcessor() {

    }

    @Override
    public String processQuery(String query) {
        return query;
    }
    
    public List<List<String>> fullProcessQuery(String query) {
    	return new ArrayList<>();
    }

}
