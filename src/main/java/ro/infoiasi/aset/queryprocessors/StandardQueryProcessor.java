
package ro.infoiasi.aset.queryprocessors;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class StandardQueryProcessor implements QueryProcessor {
    
	private final StanfordCoreNLP pipe;
	
    // Lemmatizes words
    protected StandardQueryProcessor() throws Exception {
        // throws Exception() until I figure out what kind of exception
        // it can really throw
    	Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        pipe = new StanfordCoreNLP(props);
    }
    
    @Override
    public String processQuery(String query) {
        StringBuilder lemmatizedText = new StringBuilder();
        Annotation document = new Annotation(query);
        this.pipe.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
            	lemmatizedText.append(token.get(LemmaAnnotation.class));
            	if(token.get(LemmaAnnotation.class).matches("\\w+")) {
            		lemmatizedText.append(" ");
            	}
            }
        }
        return lemmatizedText.toString().trim();
    }
    
    public List<List<String>> fullProcessQuery(String query) {
    	return new ArrayList<>();
    }
    
}
