package ro.infoiasi.aset.queryprocessors;

public class QueryProcessorFactory {

    public static QueryProcessor initializeStandardQueryProcessor() {
        try {
            return new StandardQueryProcessor();
        } catch (Exception ex) {
            ex.printStackTrace();
            return new NullQueryProcessor();
        }
    }

    public static QueryProcessor initializeFullQueryProcessor() {
        try {
            return new FullQueryProcessor();
        } catch (Exception ex) {
            ex.printStackTrace();
            return new NullQueryProcessor();
        }
    }

    public static QueryProcessor initializeSynonymQueryProcessor() {
        try {
            return new SynonymQueryProcessor();
        } catch (Exception ex) {
            ex.printStackTrace();
            return new NullQueryProcessor();
        }
    }

    public static QueryProcessor initializeTranslateQueryProcessor() {
        try {
            return new TranslateQueryProcessor();
        } catch (Exception ex) {
            ex.printStackTrace();
            return new NullQueryProcessor();
        }
    }

}
