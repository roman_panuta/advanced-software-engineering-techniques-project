
package ro.infoiasi.aset.queryprocessors;

import java.util.List;

public interface QueryProcessor {
    public String processQuery(String query);
    public List<List<String>> fullProcessQuery(String query);
}
