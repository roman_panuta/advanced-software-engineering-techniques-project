package ro.infoiasi.aset.queryprocessors;

public class QueryProcessingException extends Exception{

    public QueryProcessingException() {
    }

    public QueryProcessingException(String message) {
        super(message);
    }

    public QueryProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public QueryProcessingException(Throwable cause) {
        super(cause);
    }

    public QueryProcessingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
