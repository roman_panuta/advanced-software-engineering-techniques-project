
package ro.infoiasi.aset.queryprocessors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import edu.smu.tspell.wordnet.AdjectiveSatelliteSynset;
import edu.smu.tspell.wordnet.AdjectiveSynset;
import edu.smu.tspell.wordnet.AdverbSynset;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.VerbSynset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.util.CoreMap;

public class SynonymQueryProcessor implements QueryProcessor {

	// POS Tagger
	private final MaxentTagger tagger;
	// Synonym finder
	private final WordNetDatabase wordNetDatabase;
	// Tokenizer + Lemmatizer combo
	private final StanfordCoreNLP pipe;
	// Lemmatizer
	private final Morphology morph;

	// Finds alternative words for each word from the query
	protected SynonymQueryProcessor() throws Exception {
		// !!! SUPER IMPORTANT MESSAGE !!!
		// Set YOUR WordNet local database path in the row after the commented
		// rows
		// Local database path is inside 'dict' folder, where you installed
		// WordNet
		// If you installed WordNet simply in 'C:\', you don't have to change
		// anything
		//
		// P.S: don't forget to add two '\' for a single '\'
		// P.P.S: Download WordNet from here:
		// wordnetcode.princeton.edu/2.1/WordNet-2.1.exe
		// !!! END OF SUPER IMPORTANT MESSAGE !!!
		System.setProperty("wordnet.database.dir", new File("src/main/webapp/WEB-INF/resources/wordNet/dict").getAbsolutePath());
		wordNetDatabase = WordNetDatabase.getFileInstance();
		tagger = new MaxentTagger("src/main/java/ro/infoiasi/aset/queryprocessors/tagger/models/english-bidirectional-distsim.tagger");
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		pipe = new StanfordCoreNLP(props);
		morph = new Morphology();
	}

	@Override
	public List<List<String>> fullProcessQuery(String query) {
		List<TaggedWord> taggedQuery = new ArrayList<>();
		taggedQuery = tagQuery(query);
		// each element in this list contains a list of alternative words for
		// the query
		// for example, 'Birds fly' will generate one big list with two lists
		// within:
		// the first inner list will contain synonyms and hyponyms for the noun
		// 'bird',
		// the second will contain synonyms for the VERB 'fly' (that's why I
		// tagged the query)
		// every inner list will have the lemmatized word as the first element
		List<List<String>> processedQuery = new ArrayList<>();

		// Get replacement words with JAWS + WordNet local
		for (TaggedWord word : taggedQuery) {
			List<String> alternativeWords = new ArrayList<>();
			alternativeWords.add(morph.lemma(word.value(), word.tag()));
			if (word.tag().startsWith("NN")) {
				NounSynset nounSynset;
				Synset[] synsets = wordNetDatabase.getSynsets(word.value(), SynsetType.NOUN);
				for (Synset synset : synsets) {
					// add synonyms to the alternativeWord list
					nounSynset = (NounSynset) synset;
					String[] synsetElements = nounSynset.toString().split("\\[|\\]")[1].split(",");
					for (int i = 0; i < synsetElements.length; i++) {
						if (!alternativeWords.contains(synsetElements[i])) {
							alternativeWords.add(synsetElements[i]);
							System.out.println("Adding synonym for noun '" + word.value() + "': " + synsetElements[i]);
						}
					}

					// add hyponyms to the alternativeWords list
					NounSynset[] tempHyponymArray = nounSynset.getHyponyms();
					for (int i = 0; i < tempHyponymArray.length; i++) {
						String hyponymsString = tempHyponymArray[i].toString().split("\\[|\\]")[1];
						if (hyponymsString.contains(",")) {
							String[] separateHyponyms = hyponymsString.split(",");
							for (int j = 0; j < separateHyponyms.length; j++) {
								alternativeWords.add(separateHyponyms[j]);
								System.out.println(
										"Adding hyponym for noun '" + word.value() + "': " + separateHyponyms[j]);
							}
						} else {
							alternativeWords.add(hyponymsString);
							System.out.println("Adding hyponym for noun '" + word.value() + "': " + hyponymsString);
						}
					}
				}
			} else if (word.tag().startsWith("VB")) {
				// Finding alternatives for verbs
				VerbSynset verbSynset;
				Synset[] synsets = wordNetDatabase.getSynsets(word.value(), SynsetType.VERB);
				for (Synset synset : synsets) {
					verbSynset = (VerbSynset) synset;
					String[] synsetElements = verbSynset.toString().split("\\[|\\]")[1].split(",");
					for (int i = 0; i < synsetElements.length; i++) {
						if (!alternativeWords.contains(synsetElements[i])) {
							alternativeWords.add(synsetElements[i]);
							System.out.println("Adding synonym for verb '" + word.value() + "': " + synsetElements[i]);
						}
					}
				}
			} else if (word.tag().startsWith("JJ")) {
				// Finding alternatives for adjectives
				AdjectiveSynset adjectiveSynset;
				AdjectiveSatelliteSynset adjectiveSatelliteSynset;
				Synset[] synsets = wordNetDatabase.getSynsets(word.value(), SynsetType.ADJECTIVE);
				for (Synset synset : synsets) {
					adjectiveSynset = (AdjectiveSynset) synset;
					String[] synsetElements = adjectiveSynset.toString().split("\\[|\\]")[1].split(",");
					for (int i = 0; i < synsetElements.length; i++) {
						if (!alternativeWords.contains(synsetElements[i])) {
							alternativeWords.add(synsetElements[i]);
							System.out.println(
									"Adding synonym for adjective '" + word.value() + "': " + synsetElements[i]);
						}
					}
				}
				synsets = wordNetDatabase.getSynsets(word.value(), SynsetType.ADJECTIVE_SATELLITE);
				for (Synset synset : synsets) {
					adjectiveSatelliteSynset = (AdjectiveSatelliteSynset) synset;
					String[] synsetElements = adjectiveSatelliteSynset.toString().split("\\[|\\]")[1].split(",");
					for (int i = 0; i < synsetElements.length; i++) {
						if (!alternativeWords.contains(synsetElements[i])) {
							alternativeWords.add(synsetElements[i]);
							System.out.println("Adding synonym for satellite adjective '" + word.value() + "': "
									+ synsetElements[i]);
						}
					}
				}
			} else if (word.tag().startsWith("RB")) {
				// Finding alternatives for adverbs
				AdverbSynset adverbSynset;
				Synset[] synsets = wordNetDatabase.getSynsets(word.value(), SynsetType.ADVERB);
				for (Synset synset : synsets) {
					adverbSynset = (AdverbSynset) synset;
					String[] synsetElements = adverbSynset.toString().split("\\[|\\]")[1].split(",");
					for (int i = 0; i < synsetElements.length; i++) {
						if (!alternativeWords.contains(synsetElements[i])) {
							alternativeWords.add(synsetElements[i]);
							System.out
									.println("Adding synonym for adverb '" + word.value() + "': " + synsetElements[i]);
						}
					}
				}
			} else {
				System.out.println("Couldn't find anything for '" + word.value() + "'");
			}
			processedQuery.add(alternativeWords);
		}

		// processedQuery is the real result here
		return processedQuery;
	}

	@Override
	public String processQuery(String query) {
		StringBuilder lemmatizedText = new StringBuilder();
		Annotation document = new Annotation(query);
		this.pipe.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				lemmatizedText.append(token.get(LemmaAnnotation.class));
				if (token.get(LemmaAnnotation.class).matches("\\w+")) {
					lemmatizedText.append(" ");
				}
			}
		}
		return lemmatizedText.toString().trim();
	}

	private List<TaggedWord> tagQuery(String query) {

		List<TaggedWord> output = new ArrayList<>();

		// Need Reader to tag string, creating temporary file with query
		try (PrintWriter writer = new PrintWriter("query.tmp")) {
			writer.print(query);
			writer.flush();
			List<List<HasWord>> sentences = MaxentTagger.tokenizeText(new BufferedReader(new FileReader("query.tmp")));
			for (List<HasWord> sentence : sentences) {
				output.addAll(tagger.tagSentence(sentence));
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			File queryFile = new File("query.tmp");
			if (queryFile.exists()) {
				queryFile.delete();
			}
		}

		// Printing POS tags
		System.out.println("Tagged words: ");
		for (TaggedWord word : output) {
			System.out.println(word.value() + " = " + word.tag());
		}
		
		return output;
	}
	
}
