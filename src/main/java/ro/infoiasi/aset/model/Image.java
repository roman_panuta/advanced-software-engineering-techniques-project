package ro.infoiasi.aset.model;

import java.util.HashMap;
import java.util.Map;

public class Image {
	public static class MetaModel {
		public static final String DOCNO = "DOCNO";
		public static final String TITLE = "TITLE";
		public static final String DESCRIPTION = "DESCRIPTION";
		public static final String NOTES = "NOTES";
		public static final String LOCATION = "LOCATION";
		public static final String IMAGE = "IMAGE";
		public static final String THUMBNAIL = "THUMBNAIL";
		public static final String DATE = "DATE";
	}
    
	private String name;
	private Map<String, String> metadata = new HashMap<String, String>();
	
	public Image(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	
	public void addMetadata(String label, String value){
		metadata.put(label, value);
	}
	
}
