package ro.infoiasi.aset.research.out;

public class ImageInfo {
    private final int position;
    private final float score;

    public ImageInfo(int position, float score) {
        this.position = position;
        this.score = score;
    }

    public int getPosition() {
        return position;
    }

    public float getScore() {
        return score;
    }
}
