package ro.infoiasi.aset.research.out;

import ro.infoiasi.aset.research.in.ImageData;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Table {
    private final Map<String, ImageRow> data = new HashMap<>();

    public Map<String, ImageRow> getData() {
        return data;
    }

    public void addImage(ImageData imageData, Class<?> descriptorClass) {
        ImageRow imageRow = data.get(imageData.getName());
        if(imageRow == null) {
            imageRow = new ImageRow();
        }
        imageRow.addProperty(descriptorClass, new ImageInfo(imageData.getPosition(),imageData.getScore()));
        data.put(imageData.getName(), imageRow);
    }


    public Set<String> getAllImages() {
        return data.keySet();
    }

    public ImageRow getImageRow(String image) {
        return data.get(image);
    }
}
