package ro.infoiasi.aset.research.out;

import java.util.HashMap;
import java.util.Map;

public class ImageRow {
    private final Map<Class<?>, ImageInfo> data = new HashMap<>();

    public Map<Class<?>, ImageInfo> getData() {
        return data;
    }

    public void addProperty(Class<?> descriptor, ImageInfo imageInfo) {
        data.put(descriptor, imageInfo);
    }

    public ImageInfo getImageData(Class<?> descriptorClass) {
        return data.get(descriptorClass);
    }
}
