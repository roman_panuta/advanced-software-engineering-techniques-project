package ro.infoiasi.aset.research;

import java.util.List;

public class Statistics {

    public double spearmanCorelCoeff(List<Integer> ds1, List<Integer> ds2) {
        double sum = 0;
        double lower = (ds1.size() * (Math.pow(ds1.size(),2) - 1));
        for(int i = 0; i < ds1.size(); i++) {
            sum += ds1.get(i) - ds2.get(i);
        }
        return 1.0 - (6 * sum) / lower;
    }

    public boolean concordantPair(int i1, int i2) {
        return i1 == i2;
    }

    /*
    https://en.wikipedia.org/wiki/Kendall_rank_correlation_coefficient
     */
    public double kendallCorelCoeff(List<Integer> ds1, List<Integer> ds2) {
        int concPair, discPair = concPair = 0;
        for(int i = 0; i < ds1.size(); i++) {
            if(concordantPair(ds1.get(i), ds2.get(i))) {
                concPair ++;
            } else {
                discPair ++;
            }
        }
        return (double)(discPair - concPair) / (double)(discPair + concPair);
    }
}
