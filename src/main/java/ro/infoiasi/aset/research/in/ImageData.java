package ro.infoiasi.aset.research.in;

public class ImageData {
    private final String name;
    private final int position;
    private final float score;

    public ImageData(String name, int position, float score) {
        this.name = name;
        this.position = position;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    public float getScore() {
        return score;
    }
}
