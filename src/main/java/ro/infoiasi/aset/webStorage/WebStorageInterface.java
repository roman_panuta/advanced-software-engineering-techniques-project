package ro.infoiasi.aset.webStorage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WebStorageInterface {
    private static final int BUFFER_SIZE = 4096;
    private File tempFolder;
    private String webRoot;

    public WebStorageInterface(String webRoot) {
        this.webRoot = webRoot;
        String tempFolderName = System.getProperty("java.io.tmpdir") + "asetTemp";

        this.tempFolder = new File(tempFolderName);
        if (!this.tempFolder.exists()) {
            this.tempFolder.mkdirs();
        }
        System.out.println(this.tempFolder.getAbsolutePath());
        this.tempFolder.deleteOnExit();
    }

    public WebStorageInterface(String webRoot, String tempFolder) {
        this.webRoot = webRoot;
        this.tempFolder = new File(tempFolder);
        if(!this.tempFolder.exists()) {
            this.tempFolder.mkdirs();
        }
        System.out.println(this.tempFolder.getAbsolutePath());
        this.tempFolder.deleteOnExit();
    }

    public List<String> list(String path) {
        List<String> listOfFiles = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(webRoot + path).get();
            listOfFiles
                    .addAll(doc.getElementsByTag("a")
                            .stream()
                            .filter(link -> !link.text().equals("Parent Directory"))
                            .map(Element::text)
                            .collect(Collectors.toList()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return listOfFiles;
    }

    public File getFile(String filePath) throws IOException {
        final File file = File.createTempFile(filePath, null, this.tempFolder);
        file.deleteOnExit();

        URL url = new URL(webRoot + filePath);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(file);

            int bytesRead;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            System.out.println("File downloaded");
        }
        else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
            throw new IOException("No file to download");
        }

        return file;
    }

    public List<File> getFiles(String directoryPath) {
        List<File> fileList = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(webRoot + directoryPath).get();
            Elements links = doc.getElementsByTag("a");
            for (Element link : links) {
                // If not is parent directory and not directory
                if(!link.text().equals("Parent Directory") &&
                        !link.text().contains("/")) {
                    fileList.add(getFile(link.text()));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileList;
    }
}
