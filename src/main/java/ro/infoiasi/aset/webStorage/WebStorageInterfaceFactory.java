package ro.infoiasi.aset.webStorage;

import java.io.IOException;

public class WebStorageInterfaceFactory {
    private static WebStorageInterface webStorageInterface;

    public static void createNewInterface(String webRoot) {
        if(webStorageInterface == null) {
            webStorageInterface = new WebStorageInterface(webRoot);
        }
    }

    public static WebStorageInterface getInterface() {
        return webStorageInterface;
    }
}
