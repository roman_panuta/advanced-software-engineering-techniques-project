package ro.infoiasi.aset.controller;

import org.apache.lucene.index.IndexWriter;
import ro.infoiasi.aset.ApplicationContext;
import ro.infoiasi.aset.indexing.factories.IndexReaderFactory;
import ro.infoiasi.aset.indexing.factories.IndexWriterFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.net.URISyntaxException;

public class ServerContextListener implements ServletContextListener {
    public static final String EXCEPTION = "EXCEPTION";
    // Public constructor is required by servlet spec
    public ServerContextListener() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {
        String filepath = sce.getServletContext().getRealPath("/WEB-INF/config/path.properties");
        String wnPath = sce.getServletContext().getRealPath("/WEB-INF/resources/wordNet/dict/");
        try {
            ApplicationContext.init(filepath,wnPath);
        } catch (IOException e) {
            e.printStackTrace();
            sce.getServletContext().setAttribute(EXCEPTION,e);
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
        IndexReaderFactory.closeAll();
        IndexWriterFactory.closeAll();
    }
}
