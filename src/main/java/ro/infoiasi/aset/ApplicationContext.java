package ro.infoiasi.aset;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationContext {
	/*
		The path where the index directory will be created
	 */
	public static String INDEX_FOLDER;
	/*
		The path where the LIRE index directory will be created
	 */
	public static String LIRE_INDEX_FOLDER;
	/*
		The path to the image set directory
	 */
	public static String IMAGE_CLEF_FOLDER;
	/*
		The path to the metadata files
	 */
	public static String IMAGE_CLEF_METADATA_FOLDER;
	/*
		Extension to the files that should be parsed
	 */
	public static String METADATA_EXTENSION;

	public static String WN_DIR;

	/*
		Log file Location
	 */
	public static String LOG_FILE;

	/**
	 *
	 * @param propertiesFile the path to the properties file
	 * @param wnPath
	 * @throws IOException if file does not exist
	 */
	public static void init(String propertiesFile, String wnPath) throws IOException {
		Properties properties = new Properties();
		FileInputStream inputStream = new FileInputStream(new File(propertiesFile));
		properties.load(inputStream);
		INDEX_FOLDER  = properties.getProperty("INDEX_FOLDER");
		IMAGE_CLEF_FOLDER = properties.getProperty("IMAGE_CLEF_FOLDER");
		IMAGE_CLEF_METADATA_FOLDER = properties.getProperty("IMAGE_CLEF_METADATA_FOLDER");
		METADATA_EXTENSION = properties.getProperty("METADATA_EXTENSION");
		LIRE_INDEX_FOLDER = properties.getProperty("LIRE_INDEX_FOLDER");
		LOG_FILE = properties.getProperty("LOG_FILE");
		inputStream.close();
		WN_DIR = wnPath;
	}
}
