package ro.infoiasi.aset.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import ro.infoiasi.aset.util.CustomLogger;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Aspect
public class IndexGeneratorAspect {
    private Logger logger = CustomLogger.getLogger(this.getClass().getCanonicalName());

    @Around("execution (* ro.infoiasi.aset.indexing.beanUtils.startegy.ImageIndexingStrategy.getBufferedImage(..) )")
    public Object imageDeserializeAspect(final ProceedingJoinPoint pjp) throws Throwable {

        final Signature signature = pjp.getSignature();
        for(Object o: pjp.getArgs()) {
            System.out.println("Processing file: " + o);
        }
        try {
            return pjp.proceed();
        } catch (IOException ioe) {
            StringBuilder sb = new StringBuilder("ERROR: ");
            for(Object o: pjp.getArgs()) {
                sb.append("\r\n").append(o);
            }
            sb.append("\r\n");
            LogUtil.log(ioe,logger);
            LogUtil.log(sb.toString(),logger);
            throw ioe;
        }
    }

    @Around("execution (* ro.infoiasi.aset.util.batchProcessor.ThreadPool.submit(..) )")
    public Object threadPoolTaskAspect(final ProceedingJoinPoint pjp) throws Throwable {
        StringBuilder sb = new StringBuilder("");
        for(Object o: pjp.getArgs()) {
            sb.append(o.getClass().getName()).append(" ");
        }
        System.out.println("Added a new task " + sb.toString());
        System.out.println();
        return pjp.proceed();
    }
}