package ro.infoiasi.aset.aspects;

import ro.infoiasi.aset.queryprocessors.*;

public aspect QueryProcessorFactoryAspect {

	enum processorType {
		STANDARD, TRANSLATE, SYNONYM, FULL
	};

	pointcut standardProcessorInitializationRequest() : call(public static QueryProcessor
			QueryProcessorFactory.*());

	before(): standardProcessorInitializationRequest() {
		String methodSignature = thisJoinPoint.getSignature().toShortString();
		processorType ptype;
		if (methodSignature.toLowerCase().contains("synonym")) {
			ptype = processorType.SYNONYM;
		} else if (methodSignature.toLowerCase().contains("translate")) {
			ptype = processorType.TRANSLATE;
		} else if (methodSignature.toLowerCase().contains("full")) {
			ptype = processorType.FULL;
		} else {
			ptype = processorType.STANDARD;
		}
		switch (ptype) {
		case STANDARD:
			System.out.println("A standard query processor instance has been requested");
			break;
		case SYNONYM:
			System.out.println("A synonym query processor instance has been requested");
			break;
		case TRANSLATE:
			System.out.println("A translate query processor instance has been requested");
			break;
		case FULL:
			System.out.println("A full query processor instance has been requested");
			break;
		}
	}
	
}
