package ro.infoiasi.aset.aspects;

import ro.infoiasi.aset.queryprocessors.*;

public aspect QueryProcessorsAspect {
	
	pointcut checkQuery(String query) : call(public String processQuery(String))
		&& args(query) && !within(QueryProcessorsAspect);
	pointcut processorInitialization() : initialization(QueryProcessor+.new(..));
	
	String around(String query): checkQuery(query) {
		if(query == null) {
			return new String();
		}
		return proceed(query);
	}
	
	after() throwing(): processorInitialization() {
		System.out.println("Failed to initialize - returning null query processor instead\n");
	}
}
