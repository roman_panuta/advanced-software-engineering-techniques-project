package ro.infoiasi.aset.aspects;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public aspect SearchAspect {

	private Timestamp beforeMethodCall;
	
	//Aspect 1
	pointcut methodSearchTimer() : call(public List<String> ro.infoiasi.aset.main.ImageSearcherUtil.search(*));
	
	before() : methodSearchTimer() {
		System.out.println("... before methodOne");
		beforeMethodCall = new Timestamp(new Date().getTime());
		System.out.println("Before method call: " + beforeMethodCall);
	}
	
	after() : methodSearchTimer() {
		System.out.println("after methodOne ...");
		Timestamp afterMethodCall = new Timestamp(new Date().getTime());
		System.out.println("After method call: " + afterMethodCall);
		
		long diff = afterMethodCall.getTime () - beforeMethodCall.getTime ();
		System.out.println("Time execution was: " + diff/1000D + " s");
		
	}
	
	//Aspect 2
	pointcut methodSearchParameter(String searchExpresion) : 
		call(public List<String> ro.infoiasi.aset.main.ImageSearcherUtil.search(*)) && args(searchExpresion);
	
	after(String searchExpresion) : methodSearchParameter(searchExpresion){
		System.out.println("The search expresion was: " + searchExpresion);
	}
	
	//Aspect 3
	pointcut methodSearchReturnValue() : call(public List<String> ro.infoiasi.aset.main.ImageSearcherUtil.search(*));
	
	after() returning(List<String> result) : methodSearchReturnValue(){
		int total = 0;
		for(String s : result){
			total++;
			System.out.println("Image " + total + " is " + s);
		}
		System.out.println("Total number of find images is: " + total);
	}
	
}
