package ro.infoiasi.aset.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import ro.infoiasi.aset.util.CustomLogger;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("Duplicates")
@Aspect
public class TaskAspect {
    private Logger logger = CustomLogger.getLogger(this.getClass().getCanonicalName());

    @Around("execution (* ro.infoiasi.aset.util.batchProcessor.ProcessingTask+.setUp(..) )")
    public Object setUpAspect(final ProceedingJoinPoint pjp) throws Throwable {
        try {
            return pjp.proceed();
        }catch (final Exception e) {
            System.out.println("Caught an exception in setUp");
            LogUtil.log(e,logger);
            throw e;
        }
    }

    @Around("execution (* ro.infoiasi.aset.util.batchProcessor.ProcessingTask+.call(..) )")
    public Object callAspect(final ProceedingJoinPoint pjp) throws Throwable {
        try {
            return pjp.proceed();
        }catch (final Exception e) {
            System.out.println("Caught an exception in call");
            LogUtil.log(e,logger);
            throw e;
        }
    }

    @Around("execution (* ro.infoiasi.aset.util.batchProcessor.ProcessingTask+.TearDown(..) )")
    public Object tearDownAspect(final ProceedingJoinPoint pjp) throws Throwable {
        try {
            return pjp.proceed();
        }catch (final Exception e) {
            System.out.println("Caught an exception in tearDown");
            LogUtil.log(e,logger);
            throw e;
        }
    }
}