package ro.infoiasi.aset.aspects;


import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtil {
    public static void log(String s, Logger logger) {
        logger.log(Level.SEVERE, s);
    }
    public static void log(Exception e, Logger logger) {
        logger.log(Level.SEVERE,"Caught an exception: could not read image");
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        logger.log(Level.SEVERE,"==================");
        StringBuilder sb = new StringBuilder("");
        for(StackTraceElement element: stackTraceElements) {
            sb.append("\t").append(element.toString()).append("\r\n");
        }
        logger.log(Level.SEVERE,sb.toString());
        logger.log(Level.SEVERE,"==================");
    }
}
