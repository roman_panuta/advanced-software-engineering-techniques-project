package ro.infoiasi.aset.metadata;

import org.dom4j.io.SAXReader;

import java.io.InputStream;

public abstract class MetaDataReader<T> {
	protected final SAXReader reader;

	public MetaDataReader() {
		reader = new SAXReader();
	}
	
    public abstract T loadFile(String filePath);
    public abstract T loadFile(InputStream inputStream);
}
