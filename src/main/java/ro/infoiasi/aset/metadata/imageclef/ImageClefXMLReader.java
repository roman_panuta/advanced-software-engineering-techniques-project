package ro.infoiasi.aset.metadata.imageclef;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.xml.sax.InputSource;

import ro.infoiasi.aset.metadata.MetaDataReader;

public class ImageClefXMLReader extends MetaDataReader<ImageClefModel> {

    public ImageClefXMLReader() {

    }

    public ImageClefModel loadFile(String filePath) {
        File inputFile = new File(filePath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(inputFile);
            return loadFile(fis);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } finally {
            if(fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ImageClefModel loadFile(InputStream inputStream) {
        try {
            ImageClefModel model = new ImageClefModel();

            InputSource is = new InputSource(inputStream); 
            is.setEncoding("ISO-8859-1");
            Document document = reader.read(is);
            List<Node> nodes = document.selectNodes("/DOC");
            for (Node node : nodes) {
                model.setNameOnDisk(node.selectSingleNode("DOCNO").getText());
                model.setTitle(node.selectSingleNode("TITLE").getText());
                model.setDescription(node.selectSingleNode("DESCRIPTION").getText());
                model.setNotes(node.selectSingleNode("NOTES").getText());
                model.setLocation(node.selectSingleNode("LOCATION").getText());
                model.setDate(node.selectSingleNode("DATE").getText());
                model.setImage(node.selectSingleNode("IMAGE").getText());
                model.setThumbnail(node.selectSingleNode("THUMBNAIL").getText());
            }
            return model;
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return null;
    }
}
