package ro.infoiasi.aset.metadata.imageclef;

public class ImageClefModel {
	private String nameOnDisk;
	private String title;
	private String description;
	private String notes;
	private String location;
	private String date;
	private String image;
	private String thumbnail;

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getNotes() {
		return notes;
	}

	public String getLocation() {
		return location;
	}

	public String getDate() {
		return date;
	}

	public String getImage() {
		return image;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getNameOnDisk() {
		return nameOnDisk;
	}

	public void setNameOnDisk(String nameOnDisk) {
		this.nameOnDisk = nameOnDisk;
	}

}
