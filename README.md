# Welcome #

## Rules ##
* Use spaces instead of tabs for formatting *(1 tab = 4 spaces)*
* **Append** to a file whenever possible

## Check our [wiki](https://bitbucket.org/roman_panuta/advanced-software-engineering-techniques-project/wiki/Home) ##